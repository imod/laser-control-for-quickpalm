Laser control for QuickPALM
---------------------------

# History
This repository and software was initially created by [Ricardo Henriques](http://www.ucl.ac.uk/lmcb/users/ricardo-henriques) as a part of the QuickPALM software ([Henriques et al.](https://www.nature.com/articles/nmeth0510-339)). The original repository is hosted on [Google code](https://code.google.com/archive/p/quickpalm/) and released under GPLv3.

The code was further modified by Maxime Woringer in 2018 to incorporate internal/external control switch.

# Requirements
The following Python packages should be installed, for instance using `pip install <packagename>` or `conda install <packagename>` depending on your distribution of Python:
- serial
- wx

# Features
